from django.urls import path

from django.conf import settings
from django.conf.urls.static import static
from albums.views import image_upload_view

urlpatterns = [
    path('upload/', image_upload_view)
]
